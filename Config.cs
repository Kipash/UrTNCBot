﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UrTNCBot
{
    public class Config
    {
        public DiscordConfig DiscordConfig;
        public AppData AppData;

        public void SanetizeInstance()
        {
            if (DiscordConfig == null)
                DiscordConfig = new DiscordConfig();

            if (DiscordConfig.CommandPrefix == null)
                DiscordConfig.CommandPrefix = "!";
            if (DiscordConfig.Token == null)
                DiscordConfig.Token = "<insert-token>";
            if (DiscordConfig.GuildID == null)
                DiscordConfig.GuildID = "<inset-server-id>";
            if (DiscordConfig.ChannelID == null)
                DiscordConfig.ChannelID = "<inset-channel-id>";
            if (DiscordConfig.RoleID == null)
                DiscordConfig.RoleID = "<insert-role-id>";
            if (DiscordConfig.DeveloperIDs == null)
                DiscordConfig.DeveloperIDs = new[] { "0" };

            if (DiscordConfig.DefaultCfg == null)
                DiscordConfig.DefaultCfg = "ncctf.cfg";

            if (AppData == null)
                AppData = new AppData();
            if (AppData.CurrentServers == null)
                AppData.CurrentServers = new List<ServerData>();
        }

        const string ConfigPath = "appsettings.json";
        static JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            DefaultValueHandling = DefaultValueHandling.Populate,
            //NullValueHandling = NullValueHandling.Ignore,
            //MissingMemberHandling = MissingMemberHandling.Ignore,
            //ObjectCreationHandling = ObjectCreationHandling.Replace,
        };

        public static Config _Instance;
        public static Config LoadConfig()
        {
            if (File.Exists(ConfigPath))
            {
                var json = File.ReadAllText(ConfigPath);
                _Instance = JsonConvert.DeserializeObject<Config>(json, settings);
                _Instance.SanetizeInstance();
                SaveConfig();
            }
            else
            {
                _Instance = new Config();
                _Instance.SanetizeInstance();
                SaveConfig();

                Log.Write($"Config generated ({ConfigPath}), please fill in Token and other needed configuration!!!", LogCol.Green);
                Environment.Exit(0);
            }
            return _Instance;
        }

        public static void SaveConfig()
        {
            File.Delete(ConfigPath);
            var json = JsonConvert.SerializeObject(_Instance, settings);
            File.WriteAllText(ConfigPath, json);
        }
    }

    public class AppData
    {
        public List<ServerData> CurrentServers;
    }
    public class DiscordConfig
    {
        public string Token;
        public string CommandPrefix;
        public string GuildID;
        public string ChannelID;
        public string RoleID;
        public string DefaultCfg;
        public string[] DeveloperIDs;
    }
}