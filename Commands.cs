﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UrTNCBot
{
    public class Commands : ModuleBase<SocketCommandContext>
    {
        readonly CommandService service;

        static Random rnd = new Random();
        int newPass => rnd.Next(1000, 9999);

        Config config;
        public Commands(CommandService service, Config config)
        {
            this.service = service;
            this.config = config;
        }

        //[Command("add")]
        //public async Task AddServer()
        //{
        //    await ReplyAsync($"`{config.DiscordConfig.CommandPrefix}add <nick> <ip> <port> <rcon>`");
        //}
        //[Command("add")]
        //[Summary("registers a server so it can be used by the bot")]
        //public async Task AddServer([Remainder] string serverInfo)
        //{
        //    
        //
        //    //await ReplyAsync(@"\*command end\*");
        //}

        [Command("remove")]
        [Summary("Removes a known server")]
        public async Task RemoveServer([Remainder]string serverInfo)
        {
            var info = serverInfo.Split(' ');

            if (info.Length != 1)
            {
                await ReplyAsync("invalid nuber of arguments:\n`!remove <nick>`");
                return;
            }

            ServerManager.RemoveServer(info[0], (x) => ReplyAsync(x));
        }
        [Command("remove")]
        public async Task RemoveServer()
        {
            await ReplyAsync($"`{config.DiscordConfig.CommandPrefix}remove <nick>`");
        }

        [Command("list")]
        [Summary("List all servers")]
        public async Task ListServers()
        {
            await ReplyAsync("generating list...");
            await ReplyAsync(ServerManager.GetList());  
        }

        [Command("setup")]
        [Summary("Resets a server by executing main config and changes password")]
        public async Task SetupServer([Remainder]string cmdline)
        {
            var cmd = cmdline.Split(' ');
            var nick = cmd.FirstOrDefault();
            var force = cmd.Length > 1 && cmd[1] == "force";
            if (ServerManager.GetServer(nick, out ServerData data))
            {
                //await ReplyAsync($"setting up...");
                var s = UrT.Connect(data.IP, data.Port);
                if (s != null)
                {
                    var status = UrT.SendRcon(s, data.Rcon, "status");
                    var sLines = status.Split('\n');
                    if (sLines.Length > 5 && !force) //there are players on the server!
                    {
                        await ReplyAsync($":red_circle: There are players on the server! ({sLines.Length - 5} players)\nIf you want to set the server anyway, use `$setup <nick> force`");
                    }
                    else //no players
                    {
                        await Task.Delay(2000);

                        data.Password = newPass.ToString();
                        var passSetRes = UrT.SendRcon(s, data.Rcon, $"g_Password {data.Password}");
                        if (!string.IsNullOrEmpty(passSetRes))
                        {
                            await ReplyAsync($"Error:\n\n{passSetRes}");
                            return;
                        }

                        await Task.Delay(1000).ContinueWith((x) =>
                        {
                            var execRes = UrT.SendRcon(s, data.Rcon, $"exec {config.DiscordConfig.DefaultCfg}");
                            if (execRes.Contains("execing"))
                            {
                                string ip = (data.Port == 27960) ?
                                    ($"/connect {data.IP}; password {data.Password}") :
                                    ($"/connect {data.IP}:{data.Port}; password {data.Password}");
                                ReplyAsync(ip);

                                ServerManager.ClearNick(Context.Message.Author.Username);

                                data.LastUsedTime = DateTime.Now;
                                data.LastUsedBy = Context.Message.Author.Username;
                            }
                            else
                                ReplyAsync($"Error:\n\n{execRes}");
                        });
                    }
                }
                else
                    await ReplyAsync($"error, server didn't respond");
            }
            else
                await ReplyAsync($"{nick} is not added to the server list");
        }
        [Command("setup")]
        public async Task SetupServer()
        {
            await ReplyAsync($"`{config.DiscordConfig.CommandPrefix}setup <nick>`");
        }

        [Command("r")]
        [Summary("rcon shell. Execute Rcon commands on the server.")]
        public async Task RconShell([Remainder]string cmd)
        {
            var args = cmd.Split(' ');
            if(args.Length < 2)
            {
                await RconShell();
                return;
            }

            var nick = args[0];

            var list = args.ToList();
            list.RemoveAt(0);
            var rconCommands = string.Join(" ", list);

            if (ServerManager.GetServer(nick, out ServerData data))
            {
                using (var s = UrT.Connect(data.IP, data.Port))
                {
                    var response = UrT.SendRcon(s, data.Rcon, rconCommands);
                    await ReplyAsync(string.IsNullOrEmpty(response) ? "rcon executed" : response);
                    s.Close();
                }
            }
            else
            {
                await ReplyAsync($":red_circle: unknown server nickname: {nick}");
            }
        }
        [Command("r")]
        public async Task RconShell()
        {
            await ReplyAsync($"`{config.DiscordConfig.CommandPrefix}r <nick> <rcon commands>`");
        }

        [Command("getip")]
        [Summary("Get `/connect <ip>:<port>; password <pass>` for specific server")]
        public async Task GetConnection(string nick)
        {
            if (ServerManager.GetServer(nick, out ServerData data))
            {
                string ip = (data.Port == 27960) ?
                    ($"/connect {data.IP}; password {data.Password}") :
                    ($"/connect {data.IP}:{data.Port}; password {data.Password}");
                await ReplyAsync(ip);
            }
            else
                await ReplyAsync("server not found");
        }

        [Command("getip")]
        public async Task GetConnection()
        {
            await ReplyAsync($"`{config.DiscordConfig.CommandPrefix}getip <nick>`");
        }


        [Command("test")]
        [Summary("Diagnose issues with connecting to a server")]
        public async Task TestServer(string nick)
        {
            if(ServerManager.GetServer(nick, out ServerData data))
            {
                await ServerManager.TestCredentials(data.IP, data.Port, data.Rcon, (x) => ReplyAsync(x), () =>
                {
                    ReplyAsync(":green_circle: Server is OK");
                });
            }
        }

        [Command("test")]
        public async Task TestServer()
        {
            await ReplyAsync($"`{config.DiscordConfig.CommandPrefix}test <nick>`");
        }

        [Command("About")]
        [Summary("Information about the bot")]
        public async Task About()
        {
            await ReplyAsync(@"Made by Kipash. Source: <https://gitlab.com/Kipash/UrTNCBot>");
        }

        [Command("help")]
        [Summary("Get all available commands")]
        public async Task HelpAsync()
        {
            var prefix = config.DiscordConfig.CommandPrefix;

            var builder = new EmbedBuilder()
            {
                Color = new Color(206, 206, 30),
                Description = "**Available commands:**"
            };

            foreach (var module in service.Modules)
            {
                string description = null;
                foreach (var cmd in module.Commands)
                {
                    var result = await cmd.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                    {
                        if (string.IsNullOrEmpty(cmd.Summary))
                            continue;

                        description += $"{prefix}{cmd.Aliases.FirstOrDefault()} - {cmd.Summary}\n";
                    }
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }

            await ReplyAsync("", false, builder.Build());
        }
    }
}
