﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UrTNCBot
{
    public static class ServerManager
    {
        static Dictionary<string, ServerData> currentServers = new Dictionary<string, ServerData>();

        public static async Task RegiterServer(string nick, string ip, int port, string rcon, Action<string> response)//, bool disableMapChange = false)
        {
            nick = nick.ToLower();

            response("connecting...");
            await TestCredentials(ip, port, rcon, response, () => {
                if (currentServers.ContainsKey(nick))
                    response("Invalid nickname, such server is already added");
                else
                {
                    currentServers.Add(nick, new ServerData()
                    {
                        Nick = nick,
                        IP = ip,
                        Port = port,
                        Rcon = rcon
                    });

                    Config._Instance.AppData.CurrentServers = currentServers.Values.ToList();
                    Config.SaveConfig();

                    response(":green_circle: Server sucessfully added");

                    //if (!disableMapChange)
                    //{
                    //    Task.Delay(3000).ContinueWith((x) =>
                    //    {
                    //        var s = UrT.Connect(ip, port);
                    //        UrT.SendRcon(s, rcon, "map ut4_turnpike");
                    //    //Console.WriteLine(res);
                    //    s.Close();
                    //    });
                    //}
                }
            });
        }

        public static async Task TestCredentials(string ip, int port, string rcon, Action<string> onError, Action onServerValid)
        {
            
            var s = await Task.Run(() => UrT.Connect(ip, port));
            if (s == null)
            {
                onError(":red_circle: Invalid ip:port");
                return;
            }
            else
            {
                var testStatus = await Task.Delay(1000).ContinueWith((x) => UrT.GetServerInfoRcon(s, rcon));
                if (string.IsNullOrEmpty(testStatus))
                {
                    onError(":red_circle: Error, empty response from the server. Wrong port? Is the server running?");
                    return;
                }
                else if (testStatus.Contains("Bad rcon"))
                {
                    onError(":red_circle: Invalid rcon password");
                    return;
                }
                else
                {
                    onServerValid();
                }
            }
        }

        public static void RemoveServer(string nick, Action<string> response)
        {
            nick = nick.ToLower();
            if (currentServers.ContainsKey(nick))
            {
                currentServers.Remove(nick);
                response(":green_circle: Removed");
            }
            else
            {
                response($":red_circle: Server \"{nick}\" is not added and thefore can't be removed");
            }
        }

        public static string GetList()
        {
            string Max(string s, int max)
            {
                if (s.Length > max)
                    return s.Substring(0, max);
                else
                    return s;
            }

            string finalResponse = $"**Server list:** `{currentServers.Count} servers`\n";
            finalResponse += $"`{"ID",-4}| {"NAME", -30}| {"PASS", -6}| {"REQUESTED", -20}| {"STATUS", -4}`\n";

            foreach (var x in currentServers.OrderBy(x => x.Key))
            {
                var s = x.Value;
                bool isValid = true;

                TestCredentials(s.IP, s.Port, s.Rcon, (x) => isValid = false, () => isValid = true);

                var statusIcon = isValid ? ":green_circle:" : ":red_circle:";
                var pass = string.IsNullOrEmpty(s.Password) ? "XXXX" : s.Password;
                var reqName = Max(s.LastUsedBy, 10);
                var reqAgo = (int)DateTime.Now.Subtract(s.LastUsedTime).TotalMinutes;
                if (reqAgo > 99)
                    reqAgo = 99;

                var hasBeenRequested = s.LastUsedTime.Ticks != 0;
                var requested = hasBeenRequested ? $"{reqName} ({reqAgo} ago)" : "XXXX";

                finalResponse += $"`{Max(s.Nick, 4),-4}| {Max($"{s.IP}:{s.Port}", 30),-30}| {Max(pass, 6),-6}| {requested, -20}|` {statusIcon}\n";
            }

            return finalResponse;
        }

        public static bool GetServer(string nick, out ServerData data) => currentServers.TryGetValue(nick.ToLower(), out data);

        public static void ClearNick(string playerNick)
        {
            foreach(var x in currentServers.Values)
            {
                if(x.LastUsedBy == playerNick)
                {
                    x.LastUsedBy = "";
                    x.LastUsedTime = new DateTime();
                }
            }
        }
    }

    public class ServerData
    {
        public string Nick = "";

        public string IP = "";
        public int Port = 0;
        public string Rcon = "";
        public string Password = "";

        public string LastUsedBy = "";
        public DateTime LastUsedTime = new DateTime();
    }
}
