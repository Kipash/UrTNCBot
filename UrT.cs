﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UrTNCBot
{
    public class UrT
    {
        // fwrite($socket, str_repeat(chr(255), 4) . 'getservers ' . 26 . ' ' . $keywords . "\n");
        // 255 x 4 + $"getservers {protocol} {keywords}\n"

        public static Socket Connect(string serverIP, int port)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.ReceiveTimeout = 3000;
            socket.SendTimeout = 3000;

            try
            {
                //attempts to connect
                socket.Connect(serverIP, port);
                Log.Write($"Connected to server {serverIP} on port:{port}", LogCol.DarkGreen);
            }
            catch (Exception e)
            {
                //connect failed
                Log.Write($"<Error> Can't connect to server {serverIP} on port:{port}", LogCol.Red);
                return null;
            }

            return socket;
        }
        static byte[] GenerateByteArray(string toAppend)
        {
            byte[] bufferTemp = Encoding.ASCII.GetBytes(toAppend);
            byte[] bufferSend = new byte[bufferTemp.Length + 5];

            //intial 5 characters as per standard
            bufferSend[0] = byte.Parse("255");
            bufferSend[1] = byte.Parse("255");
            bufferSend[2] = byte.Parse("255");
            bufferSend[3] = byte.Parse("255");
            //bufferSend[4] = byte.Parse("255");
            int j = 4;

            for (int i = 0; i < bufferTemp.Length; i++)
            {
                bufferSend[j++] = bufferTemp[i];
            }

            return bufferSend;
        }

        static byte[] Send(Socket socket, string payload)
        {
            //generates payload
            byte[] toSend = GenerateByteArray(payload);

            //send rcon command and get response
            //IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            socket.Send(toSend);

            //big enough to receive response
            byte[] bufferRec = new byte[160000];

            socket.Receive(bufferRec);

            return bufferRec.Where(x => x != 0).ToArray();
        }

        /*
        public static byte[] GetServers(Socket socket)
        {
            var data = Send(socket, "getservers  68 full empty");

            var filtered = data.Skip(21).Where(x => x != 0).ToArray();
            return filtered;
        }

        public static List<Server> ParseServers(byte[] serverBytes)
        {
            int serverCount = serverBytes.Length / 7;

            List<Server> servers = new List<Server>();

            for (int i = 0; i < serverCount; i++)
            {
                byte[] ipRaw = new byte[] { serverBytes[i + 1], serverBytes[i + 2], serverBytes[i + 3], serverBytes[i + 4] };
                int port = (256 * serverBytes[i + 5]) + serverBytes[i + 6];

                var ip = new IPAddress(ipRaw);
                var s = new Server($"{ip.ToString()}:{port}");
                Console.WriteLine($"{ip.ToString()}:{port}");
                servers.Add(s);
            }

            return servers;
        }
        */

        public static string GetServerInfo(Socket socket)
        {
            try
            {
                var dataRaw = Send(socket, "getstatus");
                return Encoding.ASCII.GetString(dataRaw);
            }
            catch (Exception e)
            {
                Log.Write($"{e.Message}\n{e.StackTrace}", LogCol.Red);
                return string.Empty;
            }
        }

        public static string GetServerInfoRcon(Socket socket, string rcon)
        {
            return SendRcon(socket, rcon, "players");
        }

        public static string SendRcon(Socket socket, string rconPassword, string shell)
        {
            try
            {
                var dataRaw = Send(socket, $"rcon {rconPassword} {shell}");
                var s = Encoding.ASCII.GetString(dataRaw);
                return s.Substring(10, s.Length - 10);
            }
            catch (Exception e)
            {
                Log.Write($"{e.Message}\n{e.StackTrace}", LogCol.Red);
                return string.Empty;
            }
        }
    }

    /*
    public class Server
    {
        public string ServerId { get; set; }
        public string Address { get; set; }
        string name;
        public string Name
        {
            get
            {
                string ret_name = "";
                if (!string.IsNullOrWhiteSpace(name) && name.Length > 0)
                {
                    ret_name = name.Replace("`", @"'");
                }
                return ret_name.Length > 0 ? ret_name : "Unnamed server";
            }
            set
            {
                name = value;
            }
        }

        public string Country { get; set; }
        public string Flag { get; set; }
        public string CurrentMap { get; set; }
        public string GameType { get; set; }
        public Server(string address)
        {
            Address = address;
        }
    }
    */
}