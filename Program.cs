﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace UrTNCBot
{
    public class Program
    {
        DiscordSocketClient client;
        CommandService commands;
        IServiceProvider services;
        Config config;
        static void Main(string[] args) 
        {
            new Program()
                .RunBotAsync()
                .GetAwaiter()
                .GetResult();
        }


        public async Task RunBotAsync()
        {
            client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                AlwaysDownloadUsers = true,
                MessageCacheSize = 100,
                GatewayIntents = GatewayIntents.AllUnprivileged | GatewayIntents.MessageContent
            });
            commands = new CommandService();

            config = Config.LoadConfig();

            foreach(var x in config.AppData.CurrentServers)
            {
                await ServerManager.RegiterServer(x.Nick, x.IP, x.Port, x.Rcon, (y) => Log.Write(y, LogCol.Magenta));
            }

            services = new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .AddSingleton(config)
                .BuildServiceProvider();

            var token = config.DiscordConfig.Token;

            client.Log += ClientLog;
            await RegisterCommandsAsync();
            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();
            
            //await MessageManager.ServerViewLoop(client, config);
            await Task.Delay(-1);
        }

        Task ClientLog(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            client.MessageReceived += HandleCommandAsync;
            client.MessageReceived += HandleDMAsync;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }
        async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            var context = new SocketCommandContext(client, message);

            Console.WriteLine($"Message: {message.Content}");

            if (message.Author.IsBot)
            {
                Console.WriteLine("Ignoring: author is a bot");
                return;
            }

            if (message.Channel.Id.ToString() != config.DiscordConfig.ChannelID)
            {
                Console.WriteLine($"Ignoring: invalid channel. Current: {message.Channel.Id.ToString()}, expected is: {config.DiscordConfig.ChannelID}");
                return;
            }

            var prefix = config.DiscordConfig.CommandPrefix;

            int argPos = 0;
            if (message.HasStringPrefix(prefix, ref argPos))
            {
                var result = await commands.ExecuteAsync(context, argPos, services);
                if (!result.IsSuccess) Console.WriteLine($"Error: {result.ErrorReason}");
            }
            else
                Console.WriteLine("Message is not a command!");
        }

        async Task HandleDMAsync(SocketMessage message)
        {
            if (message.Author.IsBot)
                return;

            if (!(message.Channel is IDMChannel))
                return;

            var guild = client.Guilds.FirstOrDefault(x => x.Id.ToString() == config.DiscordConfig.GuildID);
            if (guild == null)
                return;

            if(guild.MemberCount != guild.DownloadedMemberCount)
                guild.DownloadUsersAsync();
            

            var g = (guild as IGuild);
            
            var guildUser = await g.GetUserAsync(message.Author.Id);
            if (guildUser == null)
                return;

            if (guildUser.RoleIds.Any(x => x.ToString() == config.DiscordConfig.RoleID) || 
                config.DiscordConfig.DeveloperIDs.Contains($"{guildUser.Id}"))
            {
                //handle $add by default

                var serverInfo = message.Content;
                if (serverInfo.Contains("$add"))
                {
                    string[] info;
                    if (serverInfo.Length <= 5)
                        info = new string[] { serverInfo };
                    else
                        info = serverInfo.Substring(5, serverInfo.Length - 5).Split(' ');

                    //await message.Channel.SendMessageAsync($"{info.Length}");
                    if (info.Length != 4)
                    {
                        await message.Channel.SendMessageAsync($"invalid nuber of arguments:\n`{config.DiscordConfig.CommandPrefix}add <nick> <ip> <port> <rcon>`");
                        return;
                    }

                    string nick = info[0];
                    string ip = info[1];
                    int.TryParse(info[2], out int port);
                    string rcon = info[3];

                    await ServerManager.RegiterServer(nick, ip, port, rcon, (x) =>
                    {
                        message.Channel.SendMessageAsync(x);
                    });
                }
                else 
                    await message.Channel.SendMessageAsync($"unrecognized command, try $add");
            }
        }
    }

    public class Log
    {
        public static void Write(string msg, LogCol col)
        {
            var currCol = Console.ForegroundColor;
            Console.ForegroundColor = (ConsoleColor)(int)col;
            Console.WriteLine(msg);
            Console.ForegroundColor = currCol;
        }
    }

    public enum LogCol
    {
        Black = 0,
        DarkBlue = 1,
        DarkGreen = 2,
        DarkCyan = 3,
        DarkRed = 4,
        DarkMagenta = 5,
        DarkYellow = 6,
        Gray = 7,
        DarkGray = 8,
        Blue = 9,
        Green = 10,
        Cyan = 11,
        Red = 12,
        Magenta = 13,
        Yellow = 14,
        White = 15
    }
}
